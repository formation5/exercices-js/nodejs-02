// Import http module
const request = require("request");

/*----------------------------------------------------------------------------*/

// Init api
const ipUrl = "https://api.ipify.org?format=json";
const placeUrl = "http://ip-api.com/json/";

// Init Adress value
let yourAdress = "";
const adress = process.argv[2];

// If the isn't an adress as an argument get the location of the user
if (!adress) {
    request(ipUrl, (error, response, body) => { //Request to ip finder api  
        const data = JSON.parse(body);
        request(`${placeUrl}${data.ip}`, (error, response, body2) => { // request to location finder from ip, api
            const objectAdress = JSON.parse(body2);
            yourAdress = objectAdress.city; // Assigne the location user value to yourAdress
        })
    });
} else {
    yourAdress = adress; // Assigne the argument location value to yourAdress
}


module.exports.adress = yourAdress; // Export the Adress to the module get_wethear