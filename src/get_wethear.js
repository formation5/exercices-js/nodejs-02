// Import modules
const request = require("request");
const city = require("./request_http");
const chalk = require('chalk');
require('dotenv').config();

/*----------------------------------------------------------------------------*/

// Init the api
const url = `http://api.openweathermap.org/data/2.5/weather?q=${city.adress}&units=metric&appid=`;
const apiKey = process.env.WETHEAR_API_KEY;


module.exports = request(`${url}${apiKey}`, (error, response, body) => {
    // Parse the object (response of the API) and save it in data 
    const data = JSON.parse(body);
    // Print the metrics
    console.log('\n');
    console.log(`----------------------------METEO A ${city.adress}----------------------------`);
    console.log('\n');
    console.log(chalk.white.bgGreen.bold("Temperature") + ' ' + chalk.white.bgRed.bold("Ressentit") + ' ' + chalk.white.bgCyan.bold("Temperature min") + ' ' + chalk.white.bgMagenta.bold("Temperature max") + ' ' + chalk.white.bgGrey.bold("Pression") + ' ' + chalk.white.bgBlue.bold("Humidité"));
    console.log('   ' + chalk.green.bold(data.main.temp) + '      ' + chalk.red.bold(data.main.feels_like) + '        ' + chalk.cyan.bold(data.main.temp_min) + '           ' + chalk.magenta.bold(data.main.temp_max) + '        ' + chalk.grey.bold(data.main.pressure) + '      ' + chalk.blue.bold(data.main.humidity) + ' ');
    console.log('\n');
    // Easter egg
    if (data.main.humidity > 70) {
        console.log(chalk.italic.yellow.bold("T'est qui à l'heure qui l'est monsieur ?!\n"));
    }
});